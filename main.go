	/* package main

	import (
		"database/sql"
		"fmt"

		_ "github.com/lib/pq"
	)

	const (
		host     = "localhost"
		port     = 3306
		user     = "postgres"
		password = "SysAdmin@1"
		dbname   = "postgres"
	)

	func main() {
		psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

		db, err := sql.Open("postgres", psqlconn)
		CheckError(err)

		defer db.Close()

		// insert
		// hardcoded -- - uncomment to run this
		/*insertStmt := `insert into public."Student"("Name", "RollNo") values('John', 1)`
		_, e := db.Exec(insertStmt)
		CheckError(e)

		// dynamic -- - uncomment to run this
		insertDynStmt := `insert into public."Student"("Name", "RollNo") values($1, $2)`
		_, e = db.Exec(insertDynStmt, "Jane", 2)
		CheckError(e) */

	// update   - uncomment to run this
	/*updateStmt := `update public."Student" set "Name"=$1, "RollNo"=$2 where "ID"=$3`
		_, e := db.Exec(updateStmt, "Mary", 3, 2)
	    CheckError(e) */

	// Delete
	/*deleteStmt := `delete from public."Student" where "ID"=$1`
		_, e := db.Exec(deleteStmt, 1)
		CheckError(e)
	}

	//CheckError checking error
	func CheckError(err error) {
		if err != nil {
			panic(err)
		}
	}
	*/