package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
)

type Student struct {
	//gorm.Model `json:"model"`
	Name   string `json:"name"`
	RollNo string `json:"rollNo"`
	ID     int    `json:"id"`
}

func handlerFunc(msg string) func(echo.Context) error {
	return func(c echo.Context) error {
		return c.String(http.StatusOK, msg)
	}
}

func allStudents(db *gorm.DB) func(echo.Context) error {
	return func(c echo.Context) error {
		var Students []Student
		db.Find(&Students)
		fmt.Println("{}", Students)

		return c.JSON(http.StatusOK, Students)
	}
}

func newStudent(db *gorm.DB) func(echo.Context) error {
	return func(c echo.Context) error {
		name := c.Param("name")
		rollNo := c.Param("rollNo")
		id, err := strconv.Atoi(c.Param("id"))
		if err == nil {
			db.Create(&Student{Name: name, RollNo: rollNo, ID: id})
		}
		return c.String(http.StatusOK, name+" Student successfully created")
	}
}

func deleteStudent(db *gorm.DB) func(echo.Context) error {
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))

		var Student Student
		if err == nil {
			db.Where("id = ?", id).Find(&Student)
			db.Delete(&Student)
		}
		return c.String(http.StatusOK, " Student successfully deleted")
	}
}

func updateStudent(db *gorm.DB) func(echo.Context) error {
	return func(c echo.Context) error {
		name := c.Param("name")
		rollNo := c.Param("rollNo")
		id, err := strconv.Atoi(c.Param("id"))
		var Student Student
		if err == nil {
			log.Info("inside if")

			db.Where("id=?", id).Find(&Student)

			Student.RollNo = rollNo
			Student.Name = name
			db.Save(&Student)
		}

		return c.String(http.StatusOK, name+" Student successfully updated")
	}
}

func StudentsByPage(db *gorm.DB) func(echo.Context) error {
	return func(c echo.Context) error {
		limit, _ := strconv.Atoi(c.QueryParam("limit"))
		page, _ := strconv.Atoi(c.QueryParam("page"))
		var result []Student
		db.Limit(limit).Offset(limit * (page - 1)).Find(&result)
		return c.JSON(http.StatusOK, result)
	}
}

func handleRequest(db *gorm.DB) {
	e := echo.New()

	e.GET("/Students", allStudents(db))
	e.GET("/Student", StudentsByPage(db))
	e.POST("/Student/:name/:rollNo/:id", newStudent(db))
	e.DELETE("/Student/:id", deleteStudent(db))
	e.PUT("/Student/:name/:rollNo/:id", updateStudent(db))

	e.Logger.Fatal(e.Start(":3000"))
}

func initialMigration(db *gorm.DB) {

	db.AutoMigrate(&Student{})
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "SysAdmin@1"
	dbname   = "postgres"
)

func main() {
	fmt.Println("Go ORM tutorial")
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := gorm.Open("postgres", psqlconn)
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	defer db.Close()
	initialMigration(db)
	handleRequest(db)
}
